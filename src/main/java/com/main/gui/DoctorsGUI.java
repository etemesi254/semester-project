package com.main.gui;

import com.main.db.Db;
import com.main.intf.Doctor;
import com.main.intf.Patient;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

public class DoctorsGUI
{
    private static JFrame frame;
    private static JPanel panel;
    private static Db db;
    private int index=0;

    public DoctorsGUI()
    {
        try {
            db = new Db();
        } catch (SQLException e) {
            System.out.println("Could not create database in GUI. Fatal Error");
            System.exit(1);
        }
        frame = new JFrame("Makini Hospital Doctors List");
        panel = new JPanel();
        panel.setLayout(null);
        addPatientsComboBox();
        addInputs();

        frame.setSize(570, 650);
        frame.add(panel);
        frame.setVisible(true);

    }
    private void addInputs()
    {
        try{
            ArrayList<Patient> patients =db.selectAllPatients();


            JTextField name = addInput("Doctor Name", 140);

            JTextField profession = addInput("Price", 240);

            JTextField diagnosis = addInput("Diagnosis (Sick:Yes/No)", 340);

            JButton enterButton = new JButton("Add to Db and Bill");

            enterButton.setBounds(210, 540, 200, 40);

            enterButton.addActionListener(
                    actionEvent -> {
                        String nameT = name.getText();

                        String professionT = profession.getText();

                        String diagnosisT=diagnosis.getText();

                        if ( nameT.isEmpty() || professionT.isEmpty() || diagnosisT.isEmpty())
                        {
                            // if a field is empty, don't continue
                            JOptionPane.showMessageDialog(frame, "One of the fields is empty");
                        } else
                        {
                            Patient p = patients.get(index);
                            Doctor doctor= new Doctor(nameT,professionT,diagnosisT);
                            try {
                                db.insertDoctor(doctor);

                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            JOptionPane.showMessageDialog(frame,"Successfully entered details for patient "+patients.get(index).name+" with ID "+ patients.get(index).PatientID);

                            frame.setVisible(false);
                            new LabsGUI();
                         }
                    });
            panel.add(enterButton);

        } catch (Exception e){
            e.printStackTrace();
        }
    }
    private void addPatientsComboBox(){
        try {
            ArrayList<Patient> patientsList = db.selectAllPatients();

            ArrayList<String> names = new ArrayList<>();
            for (Patient patient : patientsList) {
                names.add(String.valueOf(patient.name));
            }

            JLabel instructions= new JLabel("Patient");
            instructions.setBounds(10, 40, 500, 40);
            panel.add(instructions);

            JComboBox<Object> jb = new JComboBox<>(names.toArray());
            jb.setBounds(
                    210, 40, 200, 40
            );
            // Mahnnn lambdas are a sweet deal.
            jb.addActionListener(actionEvent -> index=jb.getSelectedIndex());

            panel.add(jb);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private JTextField addInput(String labelName, int y) {
        JLabel patientLabel = new JLabel(labelName);
        int componentWidth = 200;
        int componentHeight = 40;
        patientLabel.setBounds(10, y, componentWidth, componentHeight);
        panel.add(patientLabel);


        JTextField textField = new JTextField(3);
        int xOffset = 200;
        textField.setBounds(10 + xOffset, y, componentWidth, componentHeight);
        panel.add(textField);

        return textField;
    }
}
