package com.main.gui;

import com.main.db.Db;
import com.main.intf.Patient;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

class PatientsGUI {

    private static JFrame frame;
    private static JPanel panel;
    private static Db db;

    public PatientsGUI() {

        try {
            db = new Db();
        } catch (SQLException e) {
            System.out.println("Could not create database in GUI. Fatal Error");
            System.exit(1);
        }
        frame = new JFrame("Makini Hospital Patients List");
        panel = new JPanel();
        panel.setLayout(null);

        frame.setSize(500, 600);

        addInputs();

        addDbaseStats();

        addDeleteOptions();

        frame.setVisible(true);
    }

    private void addDbaseStats() {
        try {
            ArrayList<Patient> patientsList = db.selectAllPatients();

            addLabel("Amount Of Patients:", 10, 300);

            // shows how many patients we have in the database
            addLabel(String.valueOf(patientsList.size()), 160, 400);

            JButton detailedButton = new JButton("Show Patients details");

            // dammnnnnnnnn, lambdas
            detailedButton.addActionListener(actionEvent -> showDetailedPatients(patientsList));

            detailedButton.setBounds(10, 400, 200, 30);
            panel.add(detailedButton);


        } catch (SQLException e) {

            System.out.println("Error initializing patients\n" + e.getSQLState() + " " + e.getErrorCode() + " " + e.getMessage());
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    private void showDetailedPatients(ArrayList<Patient> patients) {

        // site https://docs.oracle.com/javase/tutorial/uiswing/components/table.html
        Vector<String> columnNames = new Vector<>(List.of(new String[]{"Patient ID", "Name", "Date Of Birth", "Report Time", "Sickness", "Assigned Personnel"}));

        JFrame newFrame = new JFrame("PatientList");
        newFrame.setSize(600, 400);

        JPanel newPanel = new JPanel();
        newPanel.setLayout(null);

        Vector<Vector<String>> data = new Vector<>();

        for (Patient patient : patients) {
            // add the fields as columns
            String[] columns = new String[]{String.valueOf(patient.PatientID), patient.name, patient.dateOfBirth.toString(),
                    patient.reportTime.toString(), patient.sickness, patient.assignedPersonnel};

            Vector<String> dt = new Vector<>(List.of(columns));

            data.add(dt);
        }

        JTable table = new JTable(data, columnNames);

        // add scrolling
        table.setFillsViewportHeight(true);


        // occupy the whole table
        newPanel.setLayout(new BorderLayout());
        newPanel.add(table.getTableHeader(), BorderLayout.PAGE_START);
        newPanel.add(table, BorderLayout.CENTER);

        //increase height
        table.setRowHeight(40);

        // make it visible
        newPanel.add(table);
        newFrame.add(newPanel);
        newFrame.setVisible(true);
    }

    private void addLabel(String labelName, int x, int y) {
        JLabel patientLabel = new JLabel(labelName);
        int componentWidth = 200;
        int componentHeight = 40;
        patientLabel.setBounds(x, y, componentWidth, componentHeight);
        panel.add(patientLabel);
    }

    private JTextField addInput(String labelName, int y) {
        JLabel patientLabel = new JLabel(labelName);
        int componentWidth = 200;
        int componentHeight = 40;
        patientLabel.setBounds(10, y, componentWidth, componentHeight);
        panel.add(patientLabel);


        JTextField textField = new JTextField(3);
        int xOffset = 250;
        textField.setBounds(10 + xOffset, y, componentWidth, componentHeight);
        panel.add(textField);

        return textField;
    }

    private void addInputs()
    {
        JLabel patientLabel = new JLabel("Add a patient to proceed to Labs");

        int componentWidth = 400;
        int componentHeight = 40;
        patientLabel.setBounds(10, 0, componentWidth, componentHeight);
        panel.add(patientLabel);

        JTextField name = addInput("Name", 40);

        JTextField dob = addInput("Date Of Birth(dd-mm-yyyy)", 140);

        JTextField sickness = addInput("Sickness", 240);

        JButton enterButton = new JButton("Add New Patient");

        enterButton.setBounds(260, 300, 200, 40);
        panel.add(enterButton);

        // dammn these lambas
        enterButton.addActionListener(actionEvent -> {
            String nameT = name.getText();
            String dobT = dob.getText();

            String sicknessT = sickness.getText();

            // retrieve text fields
            if (nameT.isEmpty() | dobT.isEmpty() | sicknessT.isEmpty()) {
                // if a field is empty, don't continue
                JOptionPane.showMessageDialog(frame, "One of the fields is empty");

            } else {
                // okay lets continue
                try {
                    Patient patient = new Patient(nameT, dobT, sicknessT);
                    db.insertPatient(patient);

                    JOptionPane.showMessageDialog(frame, "New patient " + patient.name + " added to Database, assigned ID " + patient.PatientID);

                    frame.setVisible(false);
                    // go to labs
                    new DoctorsGUI();

                } catch (Exception e) {
                    JOptionPane.showMessageDialog(frame, e.getMessage());
                }
            }

        });

        frame.add(panel);

    }

    private void addDeleteOptions() {
        try {
            ArrayList<Patient> patientsList = db.selectAllPatients();

            ArrayList<String> names = new ArrayList<>();
            for (Patient patient : patientsList) {
                names.add(patient.name);
            }

            JComboBox<Object> jb = new JComboBox<>(names.toArray());
            jb.setBounds(
                    10, 500, 150, 40
            );

            panel.add(jb);
            JButton deleteButton = new JButton("Delete Patient");

            deleteButton.setBounds(260, 500, 200, 40);

            panel.add(deleteButton);

            deleteButton.addActionListener(actionEvent -> {
                Patient p = patientsList.get(jb.getSelectedIndex());
                if (JOptionPane.showConfirmDialog(null, "Delete Details of Patient " + p.name + " ?", "WARNING",
                        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    try {
                        db.deletePatient(p);

                        JOptionPane.showMessageDialog(frame,"Patient "+p.name+" successfully deleted");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }  // do nothing

            });

        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }

    }
}