package com.oop.semesterproject;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.kordamp.bootstrapfx.BootstrapFX;

public class HomeScreen extends Application {

    ListView<Node> listView;

    VBox globalBox;
    GridPane pane = new GridPane();

    @Override
    public void start(Stage stage) throws Exception {

        pane.setMinSize(400, 400);
        pane.setAlignment(Pos.CENTER);
        pane.setPrefSize(700, 600);
        pane.setPadding(new Insets(20, 20, 20, 20));
        pane.setVgap(30);
        pane.setHgap(30);


createAdminPanel();
        createBarReportButton();
        createGraphReportButtonBar();
        createUserAccountsSettings();

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());       //(3)
        scene.getStylesheets().add(getClass().getResource("stylesheet.css").toString());

        stage.setScene(scene);

        stage.show();
    }

    private void createAdminPanel() {


        Label reportLabel  = new Label(" Administration panel");

        reportLabel.getStyleClass().add("large-home");


        pane.addRow(0,reportLabel);

        Button loginBtn = createButton("View users" ,"btn-success");
        loginBtn.setOnMouseClicked(event -> {
            Stage stage1= new Stage();
            try {
                new UserViews().start(stage1);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });


        Button productsBtn = createButton("View products" ,"btn-info");
        productsBtn.setOnMouseClicked(event -> {
            Stage stage1= new Stage();
            try {
                new ProductsView().start(stage1);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });



        ButtonBar bar = new ButtonBar();
        bar.getButtons().addAll(productsBtn,loginBtn);

        pane.addRow(1,bar);




    }

    private void createUserAccountsSettings() {
        Label reportLabel  = new Label(" User Account Settings");

        reportLabel.getStyleClass().add("large-home");

        pane.addRow(6,reportLabel);
        Button loginBtn = createButton("Login" ,"btn-success");
        loginBtn.setOnMouseClicked(event -> {
            Stage stage1= new Stage();
            try {
                new LoginScreen().start(stage1);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });

        Button signUpBtn = createButton("Sign up","btn-danger");
        signUpBtn.setOnMouseClicked(event -> {
            Stage stage1= new Stage();
            try {
                new SignUp().start(stage1);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });

        ButtonBar bar = new ButtonBar();
        bar.getButtons().addAll(loginBtn,signUpBtn);

        pane.addRow(7,bar);

    }

    private void createGraphReportButtonBar() {
        Label reportLabel  = new Label(" Pie Chart Reports");

        reportLabel.getStyleClass().add("large-home");

        pane.addRow(4,reportLabel);
        Button userReports = createButton("User login reports", "btn-info");
        userReports.setOnMouseClicked(event -> {
            Stage stage1= new Stage();
            new UserLoginsPieChart().start(stage1);
        });
        Button subcategoriesReports = createButton("Sub categories reports", "btn-danger");
        subcategoriesReports.setOnMouseClicked(event -> {
            Stage stage1= new Stage();
            new SubCategoriesReportPieCharts().start(stage1);
        });
        Button categoriesReport = createButton("Categories Report", "btn-warning");
        categoriesReport.setOnMouseClicked(event -> {
            Stage stage1= new Stage();
            new CategoriesPieChartReport().start(stage1);
        });
        Button ordersReport = createButton("Orders Report", "btn-success");
        ordersReport.setOnMouseClicked(event -> {
            Stage stage1= new Stage();
            new OrdersPieChartReport().start(stage1);
        });


        ButtonBar bar = new ButtonBar();
        bar.getButtons().addAll(userReports,subcategoriesReports,categoriesReport,ordersReport);


        pane.addRow(5,bar);

    }

    void createBarReportButton(){

        Label reportLabel  = new Label("Bar Chart Reports");

        reportLabel.getStyleClass().add("large-home");

        pane.addRow(2,reportLabel);
        Button userReports = createButton("User login reports", "btn-info");
        userReports.setOnMouseClicked(event -> {
            Stage stage1= new Stage();
            new UserLoginsBarCharts().start(stage1);
        });
        Button subcategoriesReports = createButton("Sub categories reports", "btn-danger");
        subcategoriesReports.setOnMouseClicked(event -> {
            Stage stage1= new Stage();
            new SubCategoriesReportBarChart().start(stage1);
        });
        Button categoriesReport = createButton("Categories Report", "btn-warning");
        categoriesReport.setOnMouseClicked(event -> {
            Stage stage1= new Stage();
            new CategoriesReport().start(stage1);
        });
        Button ordersReport = createButton("Orders Report", "btn-success");
        ordersReport.setOnMouseClicked(event -> {
            Stage stage1= new Stage();
            new OrdersReport().start(stage1);
        });


        ButtonBar bar = new ButtonBar();
        bar.getButtons().addAll(userReports,subcategoriesReports,categoriesReport,ordersReport);


        pane.addRow(3,bar);

    }
    Button createButton(String text, String... styleClass) {
        Button btn = new Button(text);
        btn.getStyleClass().addAll(styleClass);

        return btn;

    }
}
