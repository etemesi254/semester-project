package com.oop.semesterproject;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.commons.codec.digest.DigestUtils;
import org.kordamp.bootstrapfx.BootstrapFX;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SignUp extends Application {
    GridPane pane;

    @Override
    public void start(Stage stage) throws Exception {
        pane = new GridPane();

        pane.setMinSize(400, 400);
        pane.getStyleClass().add("panel-primary");
        pane.setAlignment(Pos.CENTER);
        pane.setPrefSize(700, 800);
        pane.setPadding(new Insets(20, 20, 20, 20));
        pane.setVgap(30);
        pane.setHgap(30);

        stage.setTitle("Create an account");
        //pane.addRow(0,new Label(),new Label("Login to screen"));
        Label lbl = new Label("Create account");
        pane.add(lbl, 2, 0);


        pane.add(new Label("Username"), 1, 1);
        TextField uname = createField(1);

        pane.add(new Label("Password"), 1, 2);
        TextField pass = new PasswordField();
        pane.add(pass,2,2);

        pane.add(new Label("email"), 1, 3);
        TextField email = createField(3);

        pane.add(new Label("Telephone Number"), 1, 4);
        TextField tel = createField(4);

        pane.add(new Label("Date of birth (YYYY-MM-DD)"), 1, 5);
        TextField dob = createField(5);

        pane.add(new Label("First Name"), 1, 6);
        TextField fName = createField(6);

        pane.add(new Label("Last name"), 1, 7);
        TextField lName = createField(7);

        pane.add(new Label("Gender"), 1, 8);
        TextField gender = createField(8);


        Button signUpBtn = new Button("Sign Up");

        signUpBtn.setMinWidth(100);
        signUpBtn.getStyleClass().add("btn-primary");
        pane.add(signUpBtn, 2, 9);

        signUpBtn.setOnMouseClicked(event -> {

            String uText = uname.getText();
            String pText = pass.getText();
            String eText = email.getText();
            String tText = tel.getText();
            String dText = dob.getText();
            String fText = fName.getText();
            String lText = lName.getText();
            String gText = gender.getText();

            if (uText.isEmpty() || pText.isEmpty() || eText.isEmpty() || tText.isEmpty() || dText.isEmpty()
                    || fText.isEmpty() || lText.isEmpty() || gText.isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("One of the fields is empty");
                alert.showAndWait()
                        .filter(response -> response == ButtonType.OK);
                return ;

            }

            createAccount(uText,pText,eText,tText,dText,fText,lText,gText,stage);



        });

        pane.add(new Label("Already have an account?"), 1, 10);
        Button loginBtn = new Button("Login");
        loginBtn.getStyleClass().add("btn-success");


        loginBtn.setMinWidth(100);
        loginBtn.setOnMouseClicked(event -> {
            try {
                new LoginScreen().start(stage);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        pane.add(loginBtn, 2, 10);

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());       //(3)
        scene.getStylesheets().add(getClass().getResource("stylesheet.css").toString());

        //scene.setRoot(pane);
        stage.setScene(scene);

        stage.show();
    }

    void createAccount(String uName,String pwd,String email,String tel,String dob,String fName, String lName, String gender,Stage stage)
    {

        String hashPassword =  DigestUtils.sha1Hex(pwd);
        String sql = "INSERT INTO web_dev.tbl_users (username,email,password,telephone,dob,first_name,last_name,gender,role) values ('%s','%s','%s','%s','%s','%s','%s','%s',1);"
                .formatted(uName,email,hashPassword,tel,dob,fName,lName,gender);


        try {
            SQLConnector conn = new SQLConnector();
            conn.execute(sql);

        } catch (SQLException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(
                    e.getMessage()
            );
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK);
        }
        try {
            new HomeScreen().start(stage);

        } catch (Exception e){
            System.out.println(e.getMessage());
            System.exit(1);
        }

    }

    TextField createField(int rowIndex) {
        TextField field = new TextField();
        pane.add(field, 2, rowIndex);
        return field;
    }
}
