package com.oop.semesterproject;

public class ItemDescription {
    public String productName;
    public String productDescription;
    public String productImage;
    public String unitPrice;
    public String availableQuantity;
    public  String createdAt;

 public ItemDescription(){
    productName="Grey Fitting Dress";
    productDescription="JPEG 2000 (JP2) is an image compression standard and coding system. It was developed from 1997 to 2000 by a Joint Photographic Experts Group committee chaired by Touradj Ebrahimi (later the JPEG president),[1] with the intention of superseding their original JPEG standard\n(created in 1992), which is based on a discrete cosine transform (DCT), with a newly\ndesigned, wavelet-based method. The standardized filename extension is .jp2 for ISO/IEC 15444-1 conforming files and .jpx for\n the extended part-2 specifications, published as ISO/IEC 15444-2. The registered MIME\ntypes are defined in RFC 3745. For ISO/IEC 15444-1 it is image/jp2.";
    productImage="corey-saldana-pIKQbdSzF_k-unsplash.jpg";
    unitPrice="1000";
    availableQuantity="20";
    createdAt="2022-06-27";
 }
}

