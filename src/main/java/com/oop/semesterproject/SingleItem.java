package com.oop.semesterproject;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.kordamp.bootstrapfx.BootstrapFX;

import java.io.FileInputStream;
import java.io.InputStream;

public class SingleItem extends Application {
    static String root = "/home/caleb/WebstormProjects/school/uploads/";

    ProductsRecord records;

    VBox box= new VBox();
    AnchorPane anchorPane = new AnchorPane();

    SingleItem(ProductsRecord record){
        records =  record;
    }

    @Override
    public void start(Stage stage) throws Exception {

        box.setSpacing(20);
        box.setPadding(new Insets(10));

        InputStream stream = new FileInputStream(root + records.getProductImage());

        Image image = new Image(stream);

        //Creating the image view
        ImageView imageView = new ImageView();


        //Setting image to the image view
        imageView.setImage(image);
        //Setting the image view parameters
        imageView.setX(10);
        imageView.setY(10);

        imageView.setFitWidth(1920);
        imageView.setFitHeight(800);
        imageView.setPreserveRatio(true);
        imageView.setSmooth(true);
        imageView.setCache(true);

        box.getChildren().add(imageView);

        addButtonFunctions();

        addDescription();

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(box);

        //scrollPane.setPrefSize(400, 400);

        AnchorPane.setTopAnchor(scrollPane, 0.);
        AnchorPane.setRightAnchor(scrollPane, 0.);
        AnchorPane.setBottomAnchor(scrollPane, 0.);
        AnchorPane.setLeftAnchor(scrollPane, 0.);

        anchorPane.getChildren().add(scrollPane);


        Scene scene = new Scene(anchorPane);

        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());       //(3)
        scene.getStylesheets().add(getClass().getResource("stylesheet.css").toString());

        stage.setTitle("Displaying Image");
        stage.setScene(scene);
        stage.sizeToScene();
        stage.show();

    }
    void  addButtonFunctions(){

        ButtonBar bar = new ButtonBar();
        bar.getButtons().add(createSingleButton("Buy"));

        bar.getButtons().add(createSingleButton("Add to Cart"));
        bar.getButtons().add(createSingleButton("Reports"));
        bar.getButtons().add(createSingleButton("Go back"));


        box.getChildren().add(bar);

    }

    void addDescription(){
        Label label = new Label("Description");
        label.getStyleClass().add("large-description");
        box.getChildren().add(label);

        Label desc = new Label(records.getProductDescription());
        desc.setPrefSize(500,100);
        box.getChildren().add(desc);
    }

    Button createSingleButton(String name){
      Button btn = new Button(name);
      btn.getStyleClass().add("btn-primary");
      return  btn;

    };
}
