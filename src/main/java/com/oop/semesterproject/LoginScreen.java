package com.oop.semesterproject;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.apache.commons.codec.digest.DigestUtils;
import org.kordamp.bootstrapfx.BootstrapFX;


import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginScreen extends Application {


    @Override
    public void start(Stage stage) throws Exception {
        GridPane pane = new GridPane();
        pane.setMinSize(400, 400);
        pane.setAlignment(Pos.CENTER);
        pane.setPrefSize(700, 600);
        pane.setPadding(new Insets(20, 20, 20, 20));
        pane.setVgap(30);
        pane.setHgap(30);

        stage.setTitle("Login Screen");
        pane.add(new Label("Login  "), 2, 0);

        pane.add(new Label("Username"), 1, 1);
        pane.add(new Label("Password"), 1, 3);

        TextField username = new TextField();
        pane.add(username, 2, 1);

        TextField password = new PasswordField();
        pane.add(password, 2, 3);

        Button loginBtn = new Button("Login");
        loginBtn.getStyleClass().add("btn-success");

        loginBtn.setMinWidth(100);
        pane.add(loginBtn, 2, 4);

        loginBtn.setOnMouseClicked(mouseEvent -> {
            String uname = username.getText();
            String pwd = password.getText();
            if (uname.isEmpty() || pwd.isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Username or password field is empty");
                alert.showAndWait()
                        .filter(response -> response == ButtonType.OK);
                return;
            }
            // send to sql connector
            verifyAndLogIn(uname, pwd, stage);

        });


        pane.add(new Label("No account"), 1, 5);
        Button signUpBtn = new Button("Sign Up");
        signUpBtn.getStyleClass().add("btn-primary");

        signUpBtn.setMinWidth(100);
        signUpBtn.setOnMouseClicked(event -> {
            try {
                new SignUp().start(stage);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        pane.add(signUpBtn, 2, 5);

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());       //(3)
        scene.getStylesheets().add(getClass().getResource("stylesheet.css").toString());

        //scene.setRoot(pane);
        stage.setScene(scene);

        stage.show();
    }

    private void verifyAndLogIn(String uname, String pwd, Stage stage) {
        try {
            SQLConnector conn = new SQLConnector();

            String pwdHash = DigestUtils.sha1Hex(pwd);
            ResultSet result = conn.executeAndReturn("SELECT * FROM web_dev.tbl_users where  username = '%s' and password='%s'".formatted(uname, pwdHash));

            if (!result.next()) {
                // user doesn't exist
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Username doesn't exist");
                alert.showAndWait()
                        .filter(response -> response == ButtonType.OK);
                return;

            }



            try {
                new HomeScreen().start(stage);

            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.exit(1);
            }


        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(
                    e.getMessage()
            );
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK);
        }
    }

    public static void main(String[] args) {
        launch();
    }
}
