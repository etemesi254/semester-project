package com.oop.semesterproject;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.sql.ResultSet;

public class UserLoginsPieChart extends Application {
    final static String austria = "Austria";
    final static String brazil = "Brazil";
    final static String france = "France";
    final static String italy = "Italy";
    final static String usa = "USA";

    final CategoryAxis xAxis = new CategoryAxis();
    final NumberAxis yAxis = new NumberAxis();

    ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();


    @Override public void start(Stage stage) {


        PieChart pieChart = new PieChart(pieChartData);

        stage.setTitle("Frequency of logged in users");


        try{
            SQLConnector connector = new SQLConnector();
            ResultSet resultSet = connector.executeAndReturn(
                    "SELECT user_id, COUNT(user_id) AS cnt FROM web_dev.tbl_userlogins GROUP BY user_id HAVING (cnt > 1)");

            while (resultSet.next()){

                String id = String.valueOf(resultSet.getInt(1));
                String name = "";
                try {
                    ResultSet t =connector.executeAndReturn("SELECT username from web_dev.tbl_users where user_id= %s".formatted(id));
                    t.next();
                    name = t.getString(1);
                } catch (Exception e){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText(e.getMessage());
                    alert.showAndWait()
                            .filter(response -> response == ButtonType.OK);
                    return ;

                }
                // SQL decided the first integer is 1
                int count = resultSet.getInt(2);

                addChart(name,name,count);
            }
        } catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK);
            return ;
        }
        Scene scene  = new Scene(pieChart,800,600);


        stage.setScene(scene);
        stage.show();
    }
    void addChart(String chartName,String name, double yValue){


        pieChartData.add(new PieChart.Data(name,yValue));

    }

    public static void main(String[] args) {
        launch(args);
    }
}