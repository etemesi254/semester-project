package com.oop.semesterproject;

///




import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.sql.ResultSet;

public class CategoriesReport extends Application {
    final static String austria = "Austria";
    final static String brazil = "Brazil";
    final static String france = "France";
    final static String italy = "Italy";
    final static String usa = "USA";

    final CategoryAxis xAxis = new CategoryAxis();
    final NumberAxis yAxis = new NumberAxis();
    final BarChart<String,Number> bc =
            new BarChart<String,Number>(xAxis,yAxis);

    @Override public void start(Stage stage) {

        bc.setTitle("FREQUENCY OF PRODUCT CATEGORIES");
        stage.setTitle("Frequency of product categories");


        try{
            SQLConnector connector = new SQLConnector();
            ResultSet resultSet = connector.executeAndReturn(
                    "SELECT category,COUNT(category) AS cnt FROM web_dev.tbl_subcategories  GROUP BY category HAVING (cnt > 1)");

            while (resultSet.next()){

                String id = String.valueOf(resultSet.getInt(1));
                String name = "";
                try {
                    ResultSet t =connector.executeAndReturn("SELECT category_name from web_dev.tbl_categories where category_id = %s".formatted(id));
                    t.next();
                    name = t.getString(1);
                } catch (Exception e){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText(e.getMessage());
                    alert.showAndWait()
                            .filter(response -> response == ButtonType.OK);
                    return ;

                }
                // SQL decided the first integer is 1
                int count = resultSet.getInt(2);
                addChart(name,name,count);
            }
        } catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK);
            return ;
        }
        Scene scene  = new Scene(bc,800,600);


        stage.setScene(scene);
        stage.show();
    }
    void addChart(String chartName,String name, double yValue){
        XYChart.Series<String,Number> series = new XYChart.Series<>();
        series.setName(chartName);
        series.getData().add(new XYChart.Data<String,Number>(name,yValue));

        bc.getData().add(series);

    }

    public static void main(String[] args) {
        launch(args);
    }
}