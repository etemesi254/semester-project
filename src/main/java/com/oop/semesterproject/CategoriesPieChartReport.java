package com.oop.semesterproject;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.sql.ResultSet;

public class CategoriesPieChartReport extends Application {


    ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();


    @Override public void start(Stage stage) {


        PieChart pieChart = new PieChart(pieChartData);

        stage.setTitle("Categories");


        try{
            SQLConnector connector = new SQLConnector();
            ResultSet resultSet = connector.executeAndReturn(
                    "SELECT category,COUNT(category) AS cnt FROM web_dev.tbl_subcategories  GROUP BY category HAVING (cnt > 1)");

            while (resultSet.next()){

                String id = String.valueOf(resultSet.getInt(1));
                String name = "";
                try {
                    ResultSet t =connector.executeAndReturn("SELECT category_name from web_dev.tbl_categories where category_id = %s".formatted(id));
                    t.next();
                    name = t.getString(1);
                } catch (Exception e){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText(e.getMessage());
                    alert.showAndWait()
                            .filter(response -> response == ButtonType.OK);
                    return ;

                }
                // SQL decided the first integer is 1
                int count = resultSet.getInt(2);

                addChart(name,name,count);
            }
        } catch (Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK);
            return ;
        }
        Scene scene  = new Scene(pieChart,800,600);


        stage.setScene(scene);
        stage.show();
    }
    void addChart(String chartName,String name, double yValue){


        pieChartData.add(new PieChart.Data(name,yValue));

    }

    public static void main(String[] args) {
        launch(args);
    }
}