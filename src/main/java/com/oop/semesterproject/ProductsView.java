package com.oop.semesterproject;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.kordamp.bootstrapfx.BootstrapFX;

import java.sql.ResultSet;

public class ProductsView extends Application {

    TableView<ProductsRecord> tableView = new TableView<>();

    ObservableList<String> options = FXCollections.observableArrayList();
    ComboBox comboBox = new ComboBox(options);

    BorderPane pane = new BorderPane();

    @Override
    public void start(Stage primaryStage) {

        addColumn("Product ID","productId");
        addColumn("Product Name", "productName");
        addColumn("Product Description", "productDescription");
        //addColumn("Produ", "email");
        addColumn("Unit Price", "unitPrice");
        addColumn("Available Quantity", "availableQuantity");
        addColumn("Created at", "createdAt");

        addHeader();
        fetchandDisplay();
        addBottomHandler(primaryStage);
        pane.setCenter(tableView);

        Scene scene = new Scene(pane);
        scene.getStylesheets().add(BootstrapFX.bootstrapFXStylesheet());       //(3)
        scene.getStylesheets().add(getClass().getResource("stylesheet.css").toString());


        primaryStage.setScene(scene);

        primaryStage.show();
    }

    private void addBottomHandler(Stage stage) {
        HBox box = new HBox();
        box.setPadding(new Insets(10));
        box.setSpacing(40);

        Label lbl = new Label("Product ID");
        lbl.setPadding(new Insets(10));
        box.getChildren().add(lbl);

        comboBox.setMinWidth(170);
        // comboBox.getStylesheets().add("-fx-margin:10px");
        //   comboBox.(new Insets(30));
        box.getChildren().add(comboBox);

        Button viewBtn = new Button("VIEW");
        viewBtn.setOnMouseClicked(event -> {
            if (comboBox.getValue() == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("ID value is not set");
                alert.showAndWait()
                        .filter(response -> response == ButtonType.OK);
                return;
            }

            try {
                SQLConnector connector = new SQLConnector();

                ResultSet result = connector.executeAndReturn("SELECT * from web_dev.tbl_products where product_id=" + comboBox.getValue());

                if (!result.next()) {
                    return;
                }

                ProductsRecord productsRecord = new ProductsRecord();

                productsRecord.productId.setValue(String.valueOf(result.getInt(1)));

                productsRecord.productName.setValue(result.getString(2));

                productsRecord.productDescription.setValue(result.getString(3));

                productsRecord.productImage.setValue(result.getString(4));

                productsRecord.unitPrice.setValue(String.valueOf(result.getInt(5)));

                productsRecord.availableQuantity.setValue(String.valueOf(result.getInt(6)));

                productsRecord.createdAt.setValue(result.getString(8));

                tableView.getItems().add(productsRecord);

                Stage stage1 = new Stage();

                new SingleItem(productsRecord).start(stage1);

            } catch (Exception e) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText(e.getMessage());
                alert.showAndWait()
                        .filter(response -> response == ButtonType.OK);
            }


        });

        box.getChildren().add(viewBtn);

        Button btn = new Button("DELETE");
        btn.setPrefHeight(10);

        btn.setOnMouseClicked(event -> {
            if (comboBox.getValue() == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("ID value is not set");
                alert.showAndWait()
                        .filter(response -> response == ButtonType.OK);
                return;
            }
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setContentText("Are you sure you want to product  with ID " + comboBox.getValue());
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK);

            if (alert.getResult().equals(ButtonType.OK)) {
                try {
                    SQLConnector connector = new SQLConnector();
                    connector.execute("DELETE  FROM web_dev.tbl_products where product_id= " + comboBox.getValue());

                } catch (Exception e) {
                    Alert alert2 = new Alert(Alert.AlertType.CONFIRMATION);
                    alert2.setContentText(e.getMessage());
                    alert2.showAndWait()
                            .filter(response -> response == ButtonType.OK);
                }
            }
        });

        Button backBtn = new Button("Go back");

        backBtn.setOnMouseClicked(event -> {
            try {
                new HomeScreen().start(stage);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });

        box.getChildren().add(btn);
        box.getChildren().add(backBtn);
        box.setMinHeight(100);
        pane.setBottom(box);
    }

    private void addHeader() {
        Label label = new Label("Products in Database");
        //  label.setPadding(new Insets(200));
        label.setContentDisplay(ContentDisplay.CENTER);
        label.setAlignment(Pos.CENTER);
        label.setMaxWidth(Double.MAX_VALUE);
        label.setMinHeight(100);
        label.getStyleClass().addAll("large-home");
        pane.setTop(label);
    }

    void addColumn(String columnName, String accessModifier) {

        TableColumn<ProductsRecord, String> column =
                new TableColumn<>(columnName);

        column.setCellValueFactory(new PropertyValueFactory<ProductsRecord, String>(accessModifier));
        tableView.getColumns().add(column);
    }

    void fetchandDisplay() {
        try {
            SQLConnector connector = new SQLConnector();

            ResultSet result = connector.executeAndReturn("SELECT * from web_dev.tbl_products");

            while (result.next()) {

                ProductsRecord productsRecord = new ProductsRecord();

                productsRecord.productId.setValue(String.valueOf(result.getInt(1)));

                productsRecord.productName.setValue(result.getString(2));

                productsRecord.productDescription.setValue(result.getString(3));

                productsRecord.productImage.setValue(result.getString(4));

                productsRecord.unitPrice.setValue(String.valueOf(result.getInt(5)));

                productsRecord.availableQuantity.setValue(String.valueOf(result.getInt(6)));

                productsRecord.createdAt.setValue(result.getString(8));

                tableView.getItems().add(productsRecord);

                comboBox.getItems().add(String.valueOf(result.getInt(1)));


            }

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.showAndWait()
                    .filter(response -> response == ButtonType.OK);
        }
    }


}
