package com.oop.semesterproject;

import java.sql.*;

/* This class deals with paradigms relating to connecting to sql
 * and any associated data dependencies that may arise
 * */
public class SQLConnector {
    Connection conn;

    public SQLConnector() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e){
            System.out.println(e.toString());
            System.exit(1);
        }
        try{
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/web_dev","caleb","fdp1IOUWS");

        } catch (SQLException e){
            System.out.println(e.toString());
            System.exit(1);
        }

    }

    /// Get an instance to this connection
    public Connection getConn() {
        return conn;
    }
    public void execute(String query) throws SQLException {
        Statement st = conn.createStatement();
        st.execute(query);
    }

    public ResultSet executeAndReturn (String query) throws  SQLException{
        Statement st = conn.createStatement();
        return st.executeQuery(query);
    }
}
