package com.oop.semesterproject;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class ProductsRecord {

    public  ObjectProperty<String> productId = new SimpleObjectProperty<>();

    protected ObjectProperty<String> productName = new SimpleObjectProperty<>();

    protected ObjectProperty<String> productDescription = new SimpleObjectProperty<>();

    protected ObjectProperty<String> productImage = new SimpleObjectProperty<>();

    protected ObjectProperty<String> unitPrice = new SimpleObjectProperty<>();

    protected ObjectProperty<String> availableQuantity = new SimpleObjectProperty<>();

    protected ObjectProperty<String> createdAt = new SimpleObjectProperty<>();



    public String getProductId(){
        return  productId.get();
    }

    public String getProductName() {
        return productName.get();
    }

    public void setProductName(ObjectProperty<String> productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription.get();
    }

    public void setProductDescription(ObjectProperty<String> productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductImage() {
        return productImage.get();
    }

    public void setProductImage(ObjectProperty<String> productImage) {
        this.productImage = productImage;
    }

    public String getUnitPrice() {
        return unitPrice.get();
    }

    public void setUnitPrice(ObjectProperty<String> unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getAvailableQuantity() {
        return availableQuantity.get();
    }

    public void setAvailableQuantity(ObjectProperty<String> availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public String getCreatedAt() {
        return createdAt.get();
    }

    public void setCreatedAt(ObjectProperty<String> createdAt) {
        this.createdAt = createdAt;
    }
}


