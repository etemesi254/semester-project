package com.oop.semesterproject;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.adapter.JavaBeanObjectProperty;
import javafx.beans.property.adapter.JavaBeanProperty;

public class UserRecords {
    public ObjectProperty<String> userId = new SimpleObjectProperty<>();
    public ObjectProperty<String> username = new SimpleObjectProperty<>();
    public ObjectProperty<String> email = new SimpleObjectProperty<>();
    public ObjectProperty<String> telephone = new SimpleObjectProperty<>();
    public ObjectProperty<String> dob = new SimpleObjectProperty<>();
    public ObjectProperty<String> firstName = new SimpleObjectProperty<>();
    public ObjectProperty<String> lastName = new SimpleObjectProperty<>();
    public ObjectProperty<String> gender = new SimpleObjectProperty<>();

  UserRecords(){
      userId.setValue("32");
      dob.setValue("dsa");
      username.setValue("hello");
      email.setValue( "etemesicaleb@gmail.com");
      telephone.setValue( "+2#42");
      firstName.setValue("43SD");
      lastName.setValue("fd");
      gender.setValue("male");

  }
    public final String getUserId() {
        return this.userId.get().toString();
    }

    public void setUserId(ObjectProperty<String> userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username.get();
    }

    public void setUsername(ObjectProperty<String> username) {
        this.username = username;
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(ObjectProperty<String> email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone.get();
    }

    public void setTelephone(ObjectProperty<String> telephone) {
        this.telephone = telephone;
    }

    public String getDob() {
        return dob.get();
    }

    public void setDob(ObjectProperty<String> dob) {
        this.dob = dob;
    }

    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(ObjectProperty<String> first_name) {
        this.firstName = first_name;
    }

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(ObjectProperty<String> last_name) {
        this.lastName = last_name;
    }

    public String getGender() {
        return gender.get();
    }

    public void setGender(ObjectProperty<String> gender) {
        this.gender = gender;
    }
}
