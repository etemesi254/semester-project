module com.oop.semesterproject {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires java.sql;
    requires org.apache.commons.codec;

    requires com.dlsc.formsfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires mysql.connector.java;


    opens com.oop.semesterproject to javafx.fxml;
    exports com.oop.semesterproject;
}